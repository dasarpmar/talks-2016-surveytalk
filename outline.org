* Introduction
  ** Arithmetic circuits
  ** VP, VNP, and where it stands wrt P vs NP
  ** Possible approaches (PIT, GCT, tau conj, direct)
  ** Shallow circuits, depth reduction
  ** VSBR statement
  ** [AV + Koiran + Tavenas]
  ** Contrapositive
  ** Recent results (table)
  ** Recent results (blip graph)
* Depth reduction
  
